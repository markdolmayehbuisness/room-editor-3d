import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import connectDB from "./src/db/connectDB.js";

dotenv.config();

import UserRoutes from "./src/routes/UserRoutes.js";
import RoomRoutes from "./src/routes/RoomRoutes.js";

import { notFound, errorHandler } from "./src/middleware/errorMiddleware.js";

const app = express();
const port = 5000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.use(express.static("uploads"));

connectDB();

app.use("/api/users", UserRoutes);
app.use("/api/rooms", RoomRoutes);

app.get("/", (req, res) => {
  if (res) {
    res.send("API is healthy");
  } else {
    res.send("API is down");
  }
});

app.use(notFound);
app.use(errorHandler);
app.listen(port, () => console.log(`server running on port ${port}`));
