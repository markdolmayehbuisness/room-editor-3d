import mongoose from "mongoose";

const furnitureSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
    unique: true,
  },
  isTable: {
    type: Boolean,
    default: false,
  },
  tableX: {
    type: Number,
    default: 0,
  },
  tableZ: {
    type: Number,
    default: 0,
  },
  tableRotation: {
    type: Number,
    default: 0,
  },
  tableColor: {
    type: String,
    default: "#ffffff",
  },
  isChair: {
    type: Boolean,
    default: false,
  },
  chairX: {
    type: Number,
    default: 0,
  },
  chairZ: {
    type: Number,
    default: 0,
  },
  chairRotation: {
    type: Number,
    default: 0,
  },
  chairColor: {
    type: String,
    default: "#ffffff",
  },
  wallColor: {
    type: String,
    default: "#ffffff",
  },
});

const Furniture = new mongoose.model("Furniture", furnitureSchema);
export default Furniture;
