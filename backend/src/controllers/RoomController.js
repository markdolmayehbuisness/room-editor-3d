import Room from "../models/RoomModel.js";
import asyncHandler from "express-async-handler";

const getRooms = asyncHandler(async (req, res) => {
  const rooms = await Room.find();
  if (rooms) {
    return res.status(200).json({
      rooms: rooms,
    });
  }
});

const CreateRoom = asyncHandler(async (req, res) => {
  const {
    user,
    isTable,
    tableX,
    tableZ,
    tableRotation,
    tableColor,
    isChair,
    chairX,
    chairZ,
    chairRotation,
    chairColor,
    wallColor,
  } = req.body;

  const roomExists = await Room.findOne({ user });

  if (roomExists) {
    res.status(400);
    throw new Error("room already exists");
  }

  const room = await Room.create({
    user,
    isTable,
    tableX,
    tableZ,
    tableRotation,
    tableColor,
    isChair,
    chairX,
    chairZ,
    chairRotation,
    chairColor,
    wallColor,
  });

  if (room) {
    res.status(201).json({
      _id: room._id,
      Table: {
        isTable: room.isTable,
        tableX: room.tableX,
        tableZ: room.tableZ,
        tableRotation: room.tableRotation,
        tableColor: room.tableColor,
      },
      Chair: {
        isChair: room.isChair,
        chairX: room.chairX,
        chairZ: room.chairZ,
        chairRotation: room.chairRotation,
        chairColor: room.chairColor,
      },
      Wall: {
        wallColor: room.wallColor,
      },
      user: req.body.user,
    });
  } else {
    res.status(400);
    throw new Error("Invalid room data");
  }
});

const updateRoom = asyncHandler(async (req, res) => {
  const user = req.params.id;
  const room = await Room.findOne({ user: user }).populate("user");

  if (room) {
    room.tableX = req.body.tableX || room.tableX;
    room.tableZ = req.body.tableZ || room.tableZ;
    room.tableRotation = req.body.tableRotation || room.tableRotation;
    room.isTable = req.body.isTable || room.isTable;
    room.tableColor = req.body.tableColor || room.tableColor;

    room.chairX = req.body.chairX || room.chairX;
    room.chairZ = req.body.chairZ || room.chairZ;
    room.chairRotation = req.body.chairRotation || room.chairRotation;
    room.isChair = req.body.isChair || room.isChair;
    room.chairColor = req.body.chairColor || room.chairColor;
    room.wallColor = req.body.wallColor || room.wallColor;

    const updatedRoom = await room.save();

    res.json({
      _id: updatedRoom._id,
      Table: {
        isTable: updatedRoom.isTable,
        tableX: updatedRoom.tableX,
        tableZ: updatedRoom.tableZ,
        tableRotation: updatedRoom.tableRotation,
        tableColor: updatedRoom.tableColor,
      },
      Chair: {
        isChair: updatedRoom.isChair,
        chairX: updatedRoom.chairX,
        chairZ: updatedRoom.chairZ,

        chairRotation: updatedRoom.chairRotation,
        chairColor: updatedRoom.chairColor,
      },
      Wall: {
        wallColor: updatedRoom.wallColor,
      },
    });
  } else {
    res.status(404);
    throw new Error("room not found");
  }
});

const getRoomById = asyncHandler(async (req, res) => {
  const user = req.params.id;
  const room = await Room.findOne({ user: user }).populate("user");
  if (room) {
    res.json({
      _id: room._id,
      user: room.user,
      Table: {
        isTable: room.isTable,
        tableX: room.tableX,
        tableZ: room.tableZ,
        tableRotation: room.tableRotation,
        tableColor: room.tableColor,
      },
      Chair: {
        isChair: room.isChair,
        chairX: room.chairX,
        chairZ: room.chairZ,
        chairRotation: room.chairRotation,
        chairColor: room.chairColor,
      },
      Wall: {
        wallColor: room.wallColor,
      },
    });
  } else {
    res.status(404);
    throw new Error("room not found");
  }
});

export { getRoomById, getRooms, CreateRoom, updateRoom };
