import express from "express";
import {
  CreateRoom,
  updateRoom,
  getRoomById,
  getRooms,
} from "../controllers/RoomController.js";

import { protect, admin } from "../middleware/authMiddleware.js";

const router = express.Router();

router.route("/").post(CreateRoom).get(protect, getRooms);
router.route("/:id").get(protect, getRoomById).put(protect, updateRoom);

export default router;
