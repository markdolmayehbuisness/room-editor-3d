import axios from "axios";
import React, { createContext, useState, useEffect } from "react";

export const DataContext = createContext();

export const DataProvider = ({ children }) => {
  const [tableColor, setTableColor] = useState("#744516");
  const [tableX, setTableX] = useState(0);
  const [tableZ, setTableZ] = useState(0);
  const [tableRotation, setTableRotation] = useState(0);
  const [tableExists, setTableExists] = useState(false);

  const [chairColor, setChairColor] = useState("#744516");
  const [chairX, setChairX] = useState(0);
  const [chairZ, setChairZ] = useState(0);
  const [chairRotation, setChairRotation] = useState(0);
  const [chairExists, setChairExists] = useState(false);

  const [wallColor, setWallColor] = useState();
  const token = JSON.parse(localStorage.getItem("token"));
  const postid = JSON.parse(localStorage.getItem("id"));

  const createRoom = (user) => {
    axios
      .post(`${process.env.REACT_APP_URL}/api/rooms`, {
        user: user,
      })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const updateRoom = () => {
    axios
      .put(
        `${process.env.REACT_APP_URL}/api/rooms/${postid}`,
        {
          isTable: tableExists,
          tableX: tableX,
          tableZ: tableZ,
          tableRotation: tableRotation,
          tableColor: tableColor,
          isChair: chairExists,
          chairX: chairX,
          chairZ: chairZ,
          chairRotation: chairRotation,
          chairColor: chairColor,
          wallColor: wallColor,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const getRoom = (postid, token) => {
    axios
      .get(`${process.env.REACT_APP_URL}/api/rooms/${postid}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        if (response) {
          setWallColor(response.data.Wall.wallColor);

          setTableColor(response.data.Table.tableColor);
          setTableZ(response.data.Table.tableZ);
          setTableX(response.data.Table.tableX);
          setTableRotation(response.data.Table.tableRotation);
          setTableExists(response.data.Table.isTable);

          setChairColor(response.data.Chair.chairColor);
          setChairZ(response.data.Chair.chairZ);
          setChairX(response.data.Chair.chairX);
          setChairRotation(response.data.Chair.chairRotation);
          setChairExists(response.data.Chair.isChair);
        }
        console.log(response);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <DataContext.Provider
      value={{
        tableColor,
        setTableColor,
        tableX,
        setTableX,
        tableZ,
        setTableZ,
        tableRotation,
        setTableRotation,
        tableExists,
        setTableExists,

        chairColor,
        setChairColor,
        chairX,
        setChairX,
        chairZ,
        setChairZ,
        chairRotation,
        setChairRotation,
        chairExists,
        setChairExists,

        wallColor,
        setWallColor,

        createRoom,
        updateRoom,
        getRoom,
      }}
    >
      {children}
    </DataContext.Provider>
  );
};
