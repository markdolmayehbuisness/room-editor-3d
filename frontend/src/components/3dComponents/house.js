import React, { useState, useRef, useContext } from "react";
import { Canvas, useFrame, useLoader } from "@react-three/fiber";

function House() {
  const roofColor = "#660a0a";
  return (
    <>
      <mesh rotation-y={0} position-y={1}>
        <boxBufferGeometry attach="geometry" args={[2.5, 2.5, 2.5]} />
        <meshStandardMaterial color={"#966F33"} />
      </mesh>
      <mesh position-y={2.25}>
        <boxBufferGeometry attach="geometry" args={[3.5, 0.25, 3.5]} />
        <meshStandardMaterial color={roofColor} />
      </mesh>
      <mesh position-y={1} position-x={1.5} position-z={1.6}>
        <cylinderBufferGeometry attach="geometry" args={[0.05, 0.05, 2.5]} />
        <meshStandardMaterial color={"#1d2739"} />
      </mesh>
      <mesh position-y={1} position-x={-1.5} position-z={1.6}>
        <cylinderBufferGeometry attach="geometry" args={[0.05, 0.05, 2.5]} />
        <meshStandardMaterial color={"#1d2739"} />
      </mesh>
      <mesh position-y={1} position-x={1.5} position-z={-1.6}>
        <cylinderBufferGeometry attach="geometry" args={[0.05, 0.05, 2.5]} />
        <meshStandardMaterial color={"#1d2739"} />
      </mesh>
      <mesh position-y={1} position-x={-1.5} position-z={-1.6}>
        <cylinderBufferGeometry attach="geometry" args={[0.05, 0.05, 2.5]} />
        <meshStandardMaterial color={"#1d2739"} />
      </mesh>
      <mesh position-y={0.75} position-x={0} position-z={1.25}>
        <boxBufferGeometry attach="geometry" args={[1, 2, 0.05]} />
        <meshStandardMaterial color={"brown"} />
      </mesh>
      <mesh position-y={0.95} position-x={-0.25} position-z={1.3}>
        <sphereBufferGeometry attach="geometry" args={[0.04, 32, 32]} />
        <meshStandardMaterial color={"#1d2739"} />
      </mesh>
      <mesh position-y={-0.3}>
        <boxBufferGeometry attach="geometry" args={[5, 0.2, 5]} />
        <meshStandardMaterial color={"#466600"} />
      </mesh>
    </>
  );
}

export default House;
