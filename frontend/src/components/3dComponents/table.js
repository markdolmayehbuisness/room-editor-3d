import React, { useState, useRef, useContext } from "react";
import { Canvas, useFrame, useLoader } from "@react-three/fiber";
import { DataContext } from "../../context/DataContext";
function Table() {
  const context = useContext(DataContext);

  return (
    <>
      <group
        rotation-z={0}
        rotation-y={context.tableRotation}
        rotation-x={0}
        position-x={context.tableX}
        position-z={context.tableZ}
      >
        <mesh position-y={0.72}>
          <boxBufferGeometry attach="geometry" args={[1.3, 0.1, 1.3]} />
          <meshStandardMaterial color={context.tableColor} />
        </mesh>
        <mesh position-y={0.12}>
          <boxBufferGeometry attach="geometry" args={[0.15, 1.1, 0.15]} />
          <meshStandardMaterial color={context.tableColor} />
        </mesh>
        <mesh position-y={-0.3}>
          <boxBufferGeometry attach="geometry" args={[0.45, 0.1, 0.5]} />
          <meshStandardMaterial color={context.tableColor} />
        </mesh>
      </group>
    </>
  );
}

export default Table;
