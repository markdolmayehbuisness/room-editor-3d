import React, { useState, useRef, useContext } from "react";
import { Canvas, useFrame, useLoader } from "@react-three/fiber";
import { DataContext } from "../../context/DataContext";
function Chair() {
  const context = useContext(DataContext);

  return (
    <>
      <group
        rotation-y={context.chairRotation}
        position-x={context.chairX}
        position-z={context.chairZ}
      >
        <mesh position-y={0.28}>
          <boxBufferGeometry attach="geometry" args={[0.75, 0.05, 0.75]} />
          <meshStandardMaterial color={context.chairColor} />
        </mesh>
        <mesh position-y={-0.1} position-x={0.35} position-z={0.35}>
          <boxBufferGeometry attach="geometry" args={[0.05, 0.72, 0.05]} />
          <meshStandardMaterial color={context.chairColor} />
        </mesh>
        <mesh position-y={-0.1} position-x={-0.35} position-z={0.35}>
          <boxBufferGeometry attach="geometry" args={[0.05, 0.72, 0.05]} />
          <meshStandardMaterial color={context.chairColor} />
        </mesh>
        <mesh position-y={-0.1} position-x={0.35} position-z={-0.35}>
          <boxBufferGeometry attach="geometry" args={[0.05, 0.72, 0.05]} />
          <meshStandardMaterial color={context.chairColor} />
        </mesh>
        <mesh position-y={-0.1} position-x={-0.35} position-z={-0.35}>
          <boxBufferGeometry attach="geometry" args={[0.05, 0.72, 0.05]} />
          <meshStandardMaterial color={context.chairColor} />
        </mesh>
        <mesh position-y={0.65} position-x={0} position-z={0.35}>
          <boxBufferGeometry attach="geometry" args={[0.75, 0.75, 0.05]} />
          <meshStandardMaterial color={context.chairColor} />
        </mesh>
      </group>
    </>
  );
}

export default Chair;
