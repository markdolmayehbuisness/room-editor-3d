import React, { useState, useRef, useContext, useEffect } from "react";
import { Canvas, useFrame, useLoader } from "@react-three/fiber";
import { DataContext } from "../../context/DataContext";

function Room() {
  const context = useContext(DataContext);
  const wallColor = context.wallColor;

  return (
    <>
      <group rotation-z={0} rotation-y={0} rotation-x={0}>
        <mesh position-y={-0.4} color={"red"}>
          <boxBufferGeometry attach="geometry" args={[6, 0.15, 6]} />
          <meshStandardMaterial color={wallColor} />
        </mesh>
        <mesh position-y={1.55} position-x={2.87}>
          <boxBufferGeometry attach="geometry" args={[0.3, 4.05, 6.001]} />
          <meshStandardMaterial color={wallColor} />
        </mesh>
        <mesh position-y={1.55} position-z={2.87}>
          <boxBufferGeometry attach="geometry" args={[6.001, 4.05, 0.3]} />
          <meshStandardMaterial color={wallColor} />
        </mesh>
      </group>
    </>
  );
}

export default Room;
