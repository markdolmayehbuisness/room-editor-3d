import React, { useState, useRef, useContext } from "react";
import { useNavigate } from "react-router-dom";
import "./sidebar.css";
import Table from "../../components/3dComponents/table.js";
import { OrbitControls } from "@react-three/drei";
import { Canvas } from "@react-three/fiber";
import { context } from "@react-three/drei/web/pivotControls/context";
import { DataContext } from "../../context/DataContext";

function Sidebar() {
  const context = useContext(DataContext);
  const Navigate = useNavigate();
  return (
    <div className="sidebar-container">
      <div className="sidebar-item">
        <h2>table</h2>
        <div className="sidebar-item-container">
          <div>
            <h2>color:</h2>
            <input
              type="text"
              placeholder="enter hex value"
              className="sidebar-item-input"
              onChange={(e) => {
                context.setTableColor(e.target.value);
              }}
            />
          </div>
          <div>
            <div className="position-selector-container">
              <h2 style={{ margin: "1rem 0rem" }}>position:</h2>
              <div className="x-container">
                X-Axis:
                <div
                  className="x-minus"
                  onClick={() => {
                    if (context.tableX > -2.65) {
                      context.setTableX(context.tableX - 0.1);
                    }
                  }}
                >
                  -x
                </div>
                <div
                  className="x-plus"
                  onClick={() => {
                    if (context.tableX < 2.05) {
                      context.setTableX(context.tableX + 0.1);
                    }
                  }}
                >
                  +x
                </div>
              </div>
              <div className="z-container">
                Z-Axis:
                <div
                  className="z-minus"
                  onClick={() => {
                    if (context.tableZ > -2.65) {
                      context.setTableZ(context.tableZ - 0.1);
                      console.log("hello");
                    }
                  }}
                >
                  -z
                </div>
                <div
                  className="z-plus"
                  onClick={() => {
                    if (context.tableZ < 2.05) {
                      context.setTableZ(context.tableZ + 0.1);
                    }
                  }}
                >
                  +z
                </div>
              </div>
              <div className="z-container">
                Y-Rotation:
                <div
                  className="z-minus"
                  onClick={() => {
                    context.setTableRotation(context.tableRotation - 0.1);
                  }}
                >
                  -y
                </div>
                <div
                  className="z-plus"
                  onClick={() => {
                    context.setTableRotation(context.tableRotation + 0.1);
                  }}
                >
                  +y
                </div>
              </div>
            </div>

            <div>
              <div className="sidebar-table-exists" onClick={() => {}}>
                {context.tableExists == false ? (
                  <div
                    className="sidebar-add-button"
                    onClick={() => {
                      context.setTableExists(true);
                    }}
                  >
                    Add
                  </div>
                ) : (
                  <div
                    className="sidebar-add-button"
                    onClick={() => {
                      context.setTableExists(false);
                    }}
                  >
                    Delete
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="sidebar-item">
        <h2>chair</h2>
        <div className="sidebar-item-container">
          <div>
            <h2>color:</h2>
            <input
              type="text"
              placeholder="enter hex value"
              className="sidebar-item-input"
              onChange={(e) => {
                context.setChairColor(e.target.value);
              }}
            />
          </div>
          <div>
            <div className="position-selector-container">
              <h2 style={{ margin: "1rem 0rem" }}>position:</h2>
              <div className="x-container">
                X-Axis:
                <div
                  className="x-minus"
                  onClick={() => {
                    if (context.chairX > -2.5) {
                      context.setChairX(context.chairX - 0.1);
                    }
                  }}
                >
                  -x
                </div>
                <div
                  className="x-plus"
                  onClick={() => {
                    if (context.chairX < 2.05) {
                      context.setChairX(context.chairX + 0.1);
                    }
                  }}
                >
                  +x
                </div>
              </div>
              <div className="z-container">
                Z-Axis:
                <div
                  className="z-minus"
                  onClick={() => {
                    if (context.chairZ > -2.5) {
                      context.setChairZ(context.chairZ - 0.1);
                    }
                  }}
                >
                  -z
                </div>
                <div
                  className="z-plus"
                  onClick={() => {
                    if (context.chairZ < 2.05) {
                      context.setChairZ(context.chairZ + 0.1);
                    }
                  }}
                >
                  +z
                </div>
              </div>
              <div className="z-container">
                Y-Rotation:
                <div
                  className="z-minus"
                  onClick={() => {
                    context.setChairRotation(context.chairRotation - 0.1);
                  }}
                >
                  -y
                </div>
                <div
                  className="z-plus"
                  onClick={() => {
                    context.setChairRotation(context.chairRotation + 0.1);
                  }}
                >
                  +y
                </div>
              </div>
            </div>
            <div>
              <div className="sidebar-table-exists" onClick={() => {}}>
                {context.chairExists == false ? (
                  <div
                    className="sidebar-add-button"
                    onClick={() => {
                      context.setChairExists(true);
                    }}
                  >
                    Add
                  </div>
                ) : (
                  <div
                    className="sidebar-add-button"
                    onClick={() => {
                      context.setChairExists(false);
                    }}
                  >
                    Delete
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="sidebar-item">
        <h2>walls</h2>
        <div className="sidebar-item-container">
          <div>
            <h2>color:</h2>
            <input
              type="text"
              placeholder="enter hex value"
              className="sidebar-item-input"
              onChange={(e) => {
                context.setWallColor(e.target.value);
              }}
            />
          </div>
        </div>
      </div>
      <div
        className="sidebar-submit"
        onClick={() => {
          context.updateRoom();
        }}
      >
        save data
      </div>
      <div
        className="sidebar-submit"
        onClick={() => {
          localStorage.clear();
          Navigate("/");
          window.location.reload();
        }}
      >
        logout
      </div>
    </div>
  );
}

export default Sidebar;
