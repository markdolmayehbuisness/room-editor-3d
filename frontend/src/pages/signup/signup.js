import React, { useState, useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import "./signup.css";
import { Canvas } from "@react-three/fiber";
import { OrbitControls } from "@react-three/drei";
import axios from "axios";
import { DataContext } from "../../context/DataContext";

import House from "../../components/3dComponents/house.js";

function Signup() {
  useEffect(() => {
    if (localStorage.getItem("token")) {
      Navigate("/home");
    }
  });
  const context = useContext(DataContext);
  const Navigate = useNavigate();

  const loginFunction = () => {
    axios
      .post(`${process.env.REACT_APP_URL}/api/users/login`, {
        email: email,
        password: pass,
      })
      .then(async (response) => {
        if (response) {
          setUserData(response.data);
          console.log(response);
          localStorage.setItem(
            "token",
            JSON.stringify(await response.data.token)
          );
          localStorage.setItem("id", JSON.stringify(await response.data._id));
          Navigate("/home");
        }

        // console.log(response.data);
      })
      .catch((error) => {
        // console.error(error);
      });
  };

  const handleSubmit = () => {
    if (!login) {
      axios
        .post(`${process.env.REACT_APP_URL}/api/users`, {
          name: name,
          email: email,
          password: pass,
        })
        .then((response) => {
          if (response) {
            context.createRoom(response.data._id);
            loginFunction();
          }
          // console.log(response);
        })
        .catch((error) => {
          // console.error(error);
        });
    } else {
      loginFunction();
    }
  };
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [login, setLogin] = useState(false);
  const [userData, setUserData] = useState([]);

  return (
    <div className="signup">
      <div className="signup-left">
        <h1 className="signup-title">3D ROOM BUILDER</h1>
        <div className="su-canvas-container">
          <Canvas camera={{ position: [3, 2, 5] }} className="signup-canvas">
            <OrbitControls
              enablePan={true}
              enableZoom={false}
              enableRotate={true}
              autoRotate={true}
            />
            <ambientLight intensity={0.45} />
            <House />
          </Canvas>
        </div>
      </div>
      <div className="signup-right">
        <h1 style={{ marginBottom: "-5rem", marginTop: "5rem" }}>
          {login ? "Login" : "Sign Up!"}
        </h1>
        <div className="su-right-form">
          {login ? (
            <></>
          ) : (
            <div className="su-form-item">
              <h2>username:</h2>
              <input
                type="text"
                className="su-input"
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />
            </div>
          )}
          <div className="su-form-item">
            <h2>email:</h2>
            <input
              type="text"
              className="su-input"
              onChange={(e) => {
                setEmail(e.target.value);
              }}
            />
          </div>
          <div className="su-form-item">
            <h2>password:</h2>
            <input
              type="password"
              className="su-input"
              onChange={(e) => {
                setPass(e.target.value);
              }}
            />
          </div>
        </div>
        <button className="su-submit" onClick={handleSubmit}>
          Submit
        </button>
        {login ? (
          <h3
            className="su-login"
            onClick={() => {
              setLogin(false);
            }}
          >
            Sign Up
          </h3>
        ) : (
          <h3
            className="su-login"
            onClick={() => {
              setLogin(true);
            }}
          >
            Login
          </h3>
        )}
      </div>
    </div>
  );
}

export default Signup;
