import React, { useRef, useContext, useState, useEffect } from "react";
import "./home.css";
import { useNavigate } from "react-router-dom";
import Sidebar from "../../components/sidebar/sidebar";
import { Canvas, useThree } from "@react-three/fiber";
import { OrbitControls, PerspectiveCamera } from "@react-three/drei";
import Room from "../../components/3dComponents/room.js";
import Table from "../../components/3dComponents/table.js";
import Chair from "../../components/3dComponents/chair.js";
import { DataContext } from "../../context/DataContext";

function CameraControls() {
  const { camera } = useThree();

  // Rotate the camera on the y-axis by 20 degrees
  camera.rotation.y = Math.PI / 9;

  return null;
}

function Home() {
  const Navigate = useNavigate();

  useEffect(() => {
    if (!localStorage.getItem("token")) {
      Navigate("/");
    }
    const token = JSON.parse(localStorage.getItem("token"));
    const postid = JSON.parse(localStorage.getItem("id"));
    const fetchRoomData = async () => {
      try {
        const data = await context.getRoom(postid, token);
        // handle the retrieved data
      } catch (error) {
        // handle errors
      }
    };
    if ((token, postid)) {
      fetchRoomData();
    }
  }, []);

  const context = useContext(DataContext);
  // context.getRoom();

  return (
    <div className="home-container">
      <Sidebar />
      <Canvas
        // camera={{ fov: 75, position: [-5, 3, 4] }}
        className="home-canvas"
      >
        <PerspectiveCamera makeDefault position={[-10, 3, -2]} />
        <OrbitControls
          enablePan={true}
          enableZoom={true}
          enableRotate={true}
          autoRotate={false}
        />
        <ambientLight intensity={0.3} />
        <pointLight position={[0.75, 0.75, 0.75]} intensity={0.6} />
        <Room></Room>
        {context.tableExists == true ? <Table></Table> : <></>}
        {context.chairExists == true ? <Chair></Chair> : <></>}
      </Canvas>
    </div>
  );
}

export default Home;
